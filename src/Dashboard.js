import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import LinearAnalysis from './analysis/linear/LinearAnalysis';
import KMeans from './analysis/kmeans/KMeans';
import PCA from './analysis/pca/PCA';
import TSNE from './analysis/tsne/TSNE';
import { addOptionsActionCreator } from './reducers/options';
import axios from './http/http';
import './Dashboard.css';

class Dashboard extends Component {

  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { value } = this.state;
    
    return (
      <div className="App">
        <AppBar position="static">
          <Tabs value={value} onChange={this.handleChange}>
            <Tab label="Linear Analysis" />
            <Tab label="K-Means" />
            <Tab label="PCA" />
            <Tab label="T-SNE" />
          </Tabs>
        </AppBar>
        {value === 0 && <LinearAnalysis></LinearAnalysis>}
        {value === 1 && <KMeans></KMeans>}
        {value === 2 && <PCA></PCA>}
        {value === 3 && <TSNE></TSNE>}
      </div>
    );
  }

  componentDidMount() {
    axios.get('/options')
         .then(res => {
           this.props.addOptionsActionCreator(res.data.opts, res.data.nopts);
         });
  }

}

export default connect(_ => {return {}}, {
  addOptionsActionCreator
})(Dashboard);