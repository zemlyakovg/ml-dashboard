import { combineReducers } from 'redux';
import { options } from './options';
import { charts } from './charts';

export default combineReducers({
    options,
    charts
});