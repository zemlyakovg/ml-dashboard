const LINEAR_CHART = 'LINEAR_CHART';
const KMEANS_PLOT = 'KMEANS_PLOT';
const TSNE_PLOT = 'TSNE_PLOT';

const defaultState = {
    linear: {
        x: [],
        y: []
    },
    kmeans: {
        data: [],
        centroids: [],
        pca: []
    },
    tsne: {
        data: []
    }
}

const charts = (state = defaultState, action) => {
    switch (action.type) {
        case LINEAR_CHART: 
            return {
                ...state,
                linear: {
                    x: action.data.x,
                    y: action.data.y
                }
            }
        case KMEANS_PLOT: 
            return {
                ...state,
                kmeans: {
                    data: action.data,
                    centroids: action.centroids
                }
            }
        case TSNE_PLOT: 
            return {
                ...state,
                tsne: {
                    data: action.data
                }
            }
        default:
            return state;
    }
}

function linearChartActionCreator(data) {
    return {
        type: LINEAR_CHART,
        data
    }
}

function kmeansPlotActionCreator(data, centroids) {
    return {
        type: KMEANS_PLOT,
        data,
        centroids
    }
}

function tsnePlotActionCreator(data, centroids) {
    return {
        type: TSNE_PLOT,
        data
    }
}

export { charts, linearChartActionCreator, kmeansPlotActionCreator, tsnePlotActionCreator };