const ADD_OPTIONS = 'ADD_OPTIONS';

const options = (state = {opts: [], nopts: []}, action) => {
    switch (action.type) {
        case ADD_OPTIONS: 
            return {
                opts: action.opts,
                nopts: action.nopts
            }
        default:
            return state;
    }
}

function addOptionsActionCreator(opts, nopts) {
    return {
        type: ADD_OPTIONS,
        opts,
        nopts
    }
}

export { options, addOptionsActionCreator };