import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Chart } from 'chart.js';

const plotDefaultConfig = {
    type: 'scatter',
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                type: 'linear',
                position: 'bottom'
            }]
        }
    }
}

class TSNEPlot extends Component {

    state = {
        chart: null
    };

    render() {
        return (
            <canvas id='kmeans-plot' ref='kmeansPlot'></canvas>
        );
    }

    componentDidMount() {
        const ctx = this.refs.kmeansPlot.getContext("2d");
        this.setState({chart: new Chart(ctx, plotDefaultConfig)});
    }

    componentDidUpdate() {
        const chart = this.state.chart;
        chart.config.data.datasets = [
            {
                backgroundColor: this.dynamicColor(),
                data: this.props.data.map(item => ({x: item.x, y: item.y}))
            }
        ]
        chart.update();
    }

    dynamicColor = () => {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
     };

}

export default connect(({charts}) => ({
    data: charts.tsne.data
}))(TSNEPlot);