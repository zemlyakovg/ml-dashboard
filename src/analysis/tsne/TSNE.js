import React from 'react';
import TSNEToolbar from './TSNEToolbar';
import TSNEPlot from './TSNEPlot';

const TSNE = () => {

    return (
      <div>
        <TSNEToolbar></TSNEToolbar>
        <TSNEPlot></TSNEPlot>
      </div>
    );

}

export default TSNE;