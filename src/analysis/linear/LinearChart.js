import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Chart } from 'chart.js';

const chartDefaultConfig = {
    type: 'line',
    options: {
        responsive: true,
        fill: false,
        title: {
            display: true,
            text: 'Chart.js Line Chart'
        },
        legend: {
            display: false
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Month'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Value'
                }
            }]
        }
    }
}

class LinearChart extends Component {

    state = {
        chart: null
    };

    render() {
        return (
            <canvas id='linear-chart' ref='linearChart'></canvas>
        );
    }

    componentDidMount() {
        const ctx = this.refs.linearChart.getContext("2d");
        this.setState({chart: new Chart(ctx, chartDefaultConfig)});
    }

    componentDidUpdate() {
        const chart = this.state.chart;
        chart.config.data.datasets = [{
            data: this.props.x,
            fill: false,
            backgroundColor: 'red',
            borderColor: 'red'
        }];
        chart.config.data.labels = this.props.y;
        chart.update();
    }

}

export default connect(({charts}) => ({
    x: charts.linear.x,
    y: charts.linear.y
}))(LinearChart);