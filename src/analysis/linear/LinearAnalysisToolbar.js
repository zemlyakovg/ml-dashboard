import React, { Component } from 'react';
import { connect } from 'react-redux';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import NativeSelect from '@material-ui/core/NativeSelect';
import { linearChartActionCreator } from '../../reducers/charts';
import axios from '../../http/http';

class LinearAnalysisToolbar extends Component {

    state = {
        opts: [],
        nopts: [],
        x: '',
        y: '',
        colorCode: ''
    };

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value }, () => {
            if (this.hasAllValues()) {
                axios.get(`/linear/${this.state.x}/${this.state.y}/${this.state.colorCode}`)
                     .then(res => {
                         this.props.linearChartActionCreator(res.data)
                     });
            }
        });
    };

    hasAllValues = () => {
        return Object.values(this.state)
                     .reduce((h, v) => h && !!v, true); 
    }

    render() {
        return (
          <form onChange={this.handleChange}>
            <FormControl>
                <InputLabel htmlFor="x">X</InputLabel>
                <NativeSelect
                value={this.state.x}
                inputProps={{
                    name: 'x',
                    id: 'x',
                }}
                >
                    <option value="" />
                    {this.props.nopts.filter(o => o !== this.state.y).map(o => <option key={o} value={o}>{o}</option>)}
                </NativeSelect>
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="y">Y</InputLabel>
                <NativeSelect
                value={this.state.y}
                inputProps={{
                    name: 'y',
                    id: 'y',
                }}
                >
                    <option value="" />
                    {this.props.nopts.filter(o => o !== this.state.x).map(o => <option key={o} value={o}>{o}</option>)}
                </NativeSelect>
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="colorCode">Color Code By</InputLabel>
                <NativeSelect
                value={this.state.colorCode}
                inputProps={{
                    name: 'colorCode',
                    id: 'colorCode',
                }}
                >
                    <option value="" />
                    {this.props.opts.map(o => <option key={o} value={o}>{o}</option>)}
                </NativeSelect>
            </FormControl>
          </form>
        );
      }

}

export default connect(({options}) => ({
    opts: options.opts,
    nopts: options.nopts
}), {
    linearChartActionCreator
})(LinearAnalysisToolbar);