import React from 'react';
import LinearAnalysisToolbar from './LinearAnalysisToolbar';
import LinearChart from './LinearChart';

const LinearAnalysis = () => {

    return (
      <div>
        <LinearAnalysisToolbar></LinearAnalysisToolbar>
        <LinearChart></LinearChart>
      </div>
    );

}

export default LinearAnalysis;