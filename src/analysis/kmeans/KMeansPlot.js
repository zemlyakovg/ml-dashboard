import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Chart } from 'chart.js';

const plotDefaultConfig = {
    type: 'scatter',
    options: {
        scales: {
            xAxes: [{
                type: 'linear',
                position: 'bottom'
            }]
        }
    }
}

class KMeansPlot extends Component {

    state = {
        chart: null
    };

    render() {
        return (
            <canvas id='kmeans-plot' ref='kmeansPlot'></canvas>
        );
    }

    componentDidMount() {
        const ctx = this.refs.kmeansPlot.getContext("2d");
        this.setState({chart: new Chart(ctx, plotDefaultConfig)});
    }

    componentDidUpdate() {
        const chart = this.state.chart;
        chart.config.data.datasets = this.props.data.reduce((datasets, data, index) => {
            datasets.push({
                label: 'Cluster ' + index,
                backgroundColor: this.dynamicColor(),
                data: data.map(item => ({x: item.x, y: item.y})) 
            });
            return datasets;
        }, []);
        chart.config.data.datasets.push({
            label: 'Centroids',
            pointStyle: 'cross',
            pointRadius: 15,
            pointHoverRadius: 25,
            backgroundColor: 'red',
            borderColor: 'red',
            data: this.props.centroids
        })
        chart.config.options.tooltips.callbacks['label'] = (tooltipItem) => {
            return tooltipItem.datasetIndex < this.props.data.length ? 
                this.props.data[tooltipItem.datasetIndex][tooltipItem.index].tooltip : tooltipItem.xLabel + ', ' + tooltipItem.yLabel;
        }
        chart.update();
    }

    dynamicColor = () => {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
     };

}

export default connect(({charts}) => ({
    data: charts.kmeans.data,
    centroids: charts.kmeans.centroids
}))(KMeansPlot);