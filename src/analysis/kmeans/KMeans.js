import React from 'react';
import KMeansToolbar from './KMeansToolbar';
import KMeansPlot from './KMeansPlot';

const KMeans = () => {

    return (
      <div>
        <KMeansToolbar></KMeansToolbar>
        <KMeansPlot></KMeansPlot>
      </div>
    );

}

export default KMeans;