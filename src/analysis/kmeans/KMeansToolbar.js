import React, { Component } from 'react';
import { connect } from 'react-redux';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Slider from '@material-ui/lab/Slider';
import axios from '../../http/http';
import { kmeansPlotActionCreator } from '../../reducers/charts';
import './KMeansToolbar.css';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

class KMeansToolbar extends Component {

    state = {
        nClusters: 2,
        xs: []
    };

    handleNClusterChange = (event, nClusters) => {
        this.setState({ nClusters });
    };

    handleXsChange = event => {
        this.setState({ xs: event.target.value });
    };

    plot = () => {
        axios.get(`/kmeans/${this.state.nClusters}/${this.state.xs.join('|')}`)
             .then(res => {
                 this.props.kmeansPlotActionCreator(res.data.data, res.data.centroids);
             });
    }

    render() {
        return (
            <div className='kmeans-toolbar'>
                <div className='n-clusters'>
                    <Typography id="label">Number of clusters: {this.state.nClusters}</Typography>
                    <Slider
                        value={this.state.nClusters}
                        onChange={this.handleNClusterChange}
                        min={2}
                        max={10}
                        step={1}
                    />
                </div>
                <FormControl className='components-control'>
                    <InputLabel htmlFor="select-multiple">Components</InputLabel>
                    <Select
                        multiple
                        value={this.state.xs}
                        onChange={this.handleXsChange}
                        input={<Input id="select-multiple" />}
                        MenuProps={MenuProps}
                    >
                        {this.props.nopts.map(name => (
                            <MenuItem key={name} value={name}>
                                {name}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <Button variant="contained" color="primary" className='plot-btn' disabled={this.state.xs.length < 2} onClick={this.plot}>
                    Plot
                </Button>
            </div>
        )
    }
}


export default connect(({options}) => ({
    nopts: options.nopts
}), {
    kmeansPlotActionCreator
})(KMeansToolbar);