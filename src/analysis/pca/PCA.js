import React, { Component } from 'react';
import { connect } from 'react-redux';
import Typography from '@material-ui/core/Typography';
import KMeansToolbar from '../kmeans/KMeansToolbar';
import KMeansPlot from '../kmeans/KMeansPlot';

class PCA extends Component {

  render() {
    return (
      <div>
        <KMeansToolbar></KMeansToolbar>
        {/* <Typography id="label">PCA components: {this.props.pca}</Typography> */}
        <KMeansPlot></KMeansPlot>
      </div>
    );
  }

}

export default connect(({charts}) => ({
  pca: charts.kmeans.pca
}))(PCA);